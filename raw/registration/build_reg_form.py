CLUBS = []
with open("clubs.txt", "r") as fh:
    CLUBS = [l.strip() for l in fh] + ["Other Club"]


class HTML(object):
    def __init__(self):
        self.out = [
            "---",
            "title: Registration Form",
            "draft: false",
            "---",
            "",
            '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>',
            '<script src="/js/reg_form.js"></script>',
            '<form name="registration" method="POST" data-netlify="true" action="/registration_result">',
        ]
        self.level = 1
        self.out.append(
            f'{"~" * self.level}The details of your registration will be sent to you, including a unique registration number. Payment details will also be included in that email. Please use your registration number as a reference when paying.'
        )
        self.out.append(f'{"~" * self.level}<ul>')
        self.level += 1
        self.out.append(
            f'{"~" * self.level}<li>Payment may be made in instalments. Full payment should be received by 30 September 2022.</li>'
        )
        self.out.append(
            f'{"~" * self.level}<li>If your registration is cancelled earlier than 21 days before your date of arrival, your payment will be refunded in full except for a R50 admin fee.</li>'
        )
        self.out.append(
            f'{"~" * self.level}<li>Cancellations later than 21 days before your date of arrival will not be refunded as the full expenses will already have been incurred for the registration.</li>'
        )
        self.out.append(
            f'{"~" * self.level}<li>Accommodation bookings can be made directly with the hotel. See <a href="/accommodation">here</a>.</li>'
        )
        self.out.append(
            f'{"~" * self.level}<li>PLEASE SUBMIT A SEPARATE REGISTRATION FORM FOR EACH ATTENDEE, WHETHER THEY ARE A LION, A PARTNER IN SERVICE OR A CHILD.</li>'
        )
        self.level -= 1
        self.out.append(f'{"~" * self.level}</ul>')

    def close(self):
        self.out.append(f'{"~" * self.level}<center>')
        self.level += 1
        self.out.append(
            f'{"~" * self.level}<button type="submit">Submit Registration Form</button>'
        )
        self.level -= 1
        self.out.append(f'{"~" * self.level}</center>')
        self.level -= 1
        self.out.append("</form>")

    def render(self):
        return "\n".join([l.replace("~", "   ") for l in self.out])

    def open_containing_div(self, cls=None):
        if cls:
            cls_div = f' id="{cls}_div"'
            cls_fs = f' id="{cls}_fs"'
        else:
            cls_div = ""
            cls_fs = ""
        self.out.append(f"{'~' * self.level}<div{cls_div}>")
        self.level += 1
        self.out.append(f"{'~' * self.level}<fieldset{cls_fs} disabled>")
        self.level += 1

    def close_containing_div(self):
        self.level -= 1
        self.out.append(f"{'~' * self.level}</fieldset>")
        self.level -= 1
        self.out.append(f"{'~' * self.level}</div>")

    def open_form_item(self, tag, label, number=False):
        self.out.append(f'{"~" * self.level}<div class="form-group row">')
        self.level += 1
        self.out.append(
            f'{"~" * self.level}<label for="{tag}" class="col-sm-{"8" if number else "4"} col-form-label">'
        )
        self.level += 1
        self.out.append(f'{"~" * self.level}{label}: ')
        self.level -= 1
        self.out.extend(
            (
                f'{"~" * self.level}</label>',
                f'{"~" * self.level}<div class="col-sm-{"4" if number else "8"}">',
            )
        )
        self.level += 1

    def close_form_item(self):
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_header(self, text):
        self.out.append(f'{"~" * self.level}<h2>{text}</h2>')

    def add_label(self, tag, text, centre=False, font=None):
        self.out.append(f'{"~" * self.level}<div class="form-group row">')
        self.level += 1
        if centre:
            self.out.append(f'{"~" * self.level}<center>')
            self.level += 1
        self.out.append(
            f'{"~" * self.level}<label id={tag}>{"<h3>" if font=="b" else ""}{text}{"</h3>" if font=="b" else ""}</label>'
        )
        self.level -= 1
        if centre:
            self.out.append(f'{"~" * self.level}</center>')
            self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_text(
        self,
        tag,
        label,
        help="",
        type="text",
        cls="",
        cost=None,
        disabled=False,
    ):
        if label:
            self.open_form_item(tag, label, number=type == "number")
        if help:
            help_attr = f' aria-describedby="{tag}_help"'
        else:
            help_attr = ""
        if cost:
            cost_attr = f" cost={cost}"
        else:
            cost_attr = ""
        base_class = "form-control"
        if cls:
            class_attr = f"{base_class} {cls}"
        else:
            class_attr = base_class
        m = ""
        if type == "number":
            m = 'min="0" value="0"'
        inner = [
            f'{"~" * self.level}<input type="{type}" {m} class="{class_attr}" id="{tag}" name="{tag}"{help_attr}{cost_attr}{" disabled" if disabled else ""}>'
        ]
        if help:
            inner.append(
                f'{"~" * self.level}<small id="{tag}_help" class="form-text text-muted">'
            )
            self.level += 1
            inner.append(f'{"~" * self.level}{help}')
            self.level -= 1
            inner.append(f'{"~" * self.level}</small>')
        self.out.extend(inner)
        if label:
            self.close_form_item()

    def add_email(self, tag, label, help=""):
        self.add_text(tag, label, help=help, type="email")

    def add_checkbox(self, tag, label, help=""):
        if help:
            help_attr = f' aria-describedby="{tag}_help"'
        else:
            help_attr = ""
        self.out.append(f'{"~" * self.level}<div class="form-check">')
        self.level += 1
        self.out.extend(
            (
                f'{"~" * self.level}<input class="form-check-input" type="checkbox" value="{tag}" name="{tag}" id="{tag}"{help_attr}>',
                f'{"~" * self.level}<label class="form-check-label" for="{tag}">',
            )
        )
        self.level += 1
        self.out.append(f'{"~" * self.level}{label}')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</label>')
        if help:
            self.out.append(
                f'{"~" * self.level}<small id="{tag}_help" class="form-text text-muted">'
            )
            self.level += 1
            self.out.append(f'{"~" * self.level}{help}')
            self.level -= 1
            self.out.append(f'{"~" * self.level}</small>')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_selector(self, tag, label, items, help="", dummy_text="", required=False):
        """If dummy_text, add the text as the first entry"""
        self.open_form_item(tag, label)
        if help:
            help_attr = f' aria-describedby="{tag}_help"'
        else:
            help_attr = ""
        inner = [
            f'{"~" * self.level}<select class="form-control" id="{tag}" name="{tag}"{help_attr}{" required" if required else ""}>'
        ]
        self.level += 1
        if dummy_text:
            inner.append(f'{"~" * self.level}<option value="">{dummy_text}</option>')
        inner.extend(
            [
                f'{"~" * self.level}<option value="{item}">{item}</option>'
                for item in items
            ]
        )
        self.level -= 1
        inner.append(f'{"~" * self.level}</select>')
        if help:
            inner.append(
                f'{"~" * self.level}<small id="{tag}_help" class="form-text text-muted">'
            )
            self.level += 1
            inner.append(f'{"~" * self.level}{help}')
            self.level -= 1
            inner.append(f'{"~" * self.level}</small>')
        self.out.extend(inner)
        self.close_form_item()

    def add_radios(self, name, options):
        self.out.append(f'{"~" * self.level}<div class="form-group row">')
        self.level += 1
        for (n, (k, v)) in enumerate(options.items()):
            self.out.append(f'{"~" * self.level}<div class="form-check">')
            self.level += 1
            self.out.append(
                f'{"~" * self.level}<input class="form-check-input" type="radio" name="{name}" id="{k}" value="{k}"{" checked" if n == 0 else ""}>'
            )
            self.out.append(
                f'{"~" * self.level}<label class="form-check-label" for="{k}">'
            )
            self.level += 1
            self.out.append(f'{"~" * self.level}{v}')
            self.level -= 1
            self.out.append(f'{"~" * self.level}</label>')
            self.level -= 1
            self.out.append(f'{"~" * self.level}</div>')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_divider(self):
        self.out.append(f'{"~" * self.level}<hr>')


def make_attendee_fields(html, required=False):
    html.add_radios(
        "delegate_type",
        {
            "lion": "Delegate is a Lion",
            "pis": "Delegate is a Partner in Service or child",
        },
    )
    html.add_text(
        "first_names",
        "First Name(s)",
        "Attendee's first name or names. Please use a real name rather than a nickname - nicknames can however be used for the name badge later in this form.",
    )
    html.add_text("last_name", "Last Name")

    html.add_text(
        "name_badge",
        "Name Badge",
        help=f"The name to appear on the attendee's name badge. eg Joe Bloggs; Lion John Doe; ZC Wendy Bloggs; PDG Jane Doe; Partner in Service Joe Bloggs",
    )

    html.add_selector(
        "club",
        "Lions Club",
        CLUBS,
        help="For a Partner in Service or child, please select the Lions member's club",
        dummy_text="Please select a club",
        required=required,
    )

    html.add_text(
        "cell",
        "Cell Phone",
        "A cellphone number the attendee can be reached at if needed. This number may also be used for urgent SMSes for changes of plan during the convention.",
    )
    html.add_email("email", "Email Address")
    html.add_text(
        "dietary",
        "Dietary Requirements",
        help="Please be VERY clear with these requirements. eg halal, kosher, vegetarian, allergic to dairy",
    )
    html.add_text(
        "disability",
        "Special Access Requirements",
        help="Please indicate any requirements for wheel chair access or the like, if applicable",
    )


html = HTML()
html.add_header("Delegate's Details")
make_attendee_fields(html, required=True)
html.add_divider()
html.add_header("Total Cost")
html.add_label(
    "total_cost",
    "Total Cost: <ul><li>For Lions Members: R750</li><li>For Partners In Service and children: R575<ul><li>(Excludes the Saturday morning conference but does include lunch on Saturday)</li></ul></li></ul>",
    centre=False,
    font="b",
)
html.close()

with open("../../content/registration/_index.html", "w") as fh:
    fh.write(html.render())
print(html.render())
